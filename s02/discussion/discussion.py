# IMPORTANT

# Comments in Python are done using the "#" symbol
# One line comment

# Python Syntax
# Hello World in Python

print("Hello World")

# Indentation cause ERROR
# Semicolon (;) is NOT necessary

# Variables
# Declaring Variables (datatype are NOT needed)
# Snake case (snake_case) for multiple word variable
age = 18 
middle_initial = "G"
name1, name2, name3, name4 = "Jhon", "Ann", "Miah", "Baylon"

# Data Types
# Strings (str) - for alphanumeric and symbols
full_name = "Jane Doe"
secret_code = "P@$$w0Rd"

# Numbers (int, float, complex)
num_of_days = 365 # this is an integer
pi_approx = 3.1416 # this is a float
complex_num = 1 + 5

# Boolean (bool) - for truth values
isLearning = True
isDifficult = False

# Using our Variables
print("My name is " + full_name) 
# print("My age is " + age)

# Typecasting
# int() - converts the value to integer value
# float() - converts the value into a float value
# str() - converts the value into string

print("My age is " + str(age))
print(int(3.5))

# F-strings, add a lowercase "f" before the string and wrap the variable in {}
print(f"My age is {age}") 

# Operations
# Arithmetic Operators - performs mathematical operations
print(1 + 80)
print(28 - 12)
print(78 * 2)
print(56 / 2)
print(12 % 7)

# Assignment Operators 
num1 = 5
num1 += 7
num1 -= 6
num1 /= 1
num1 *= 2

# Comparison Operator - used to compare values (return a boolean value)
print(1 == 1)
print(12 > 20)
print(32 < 1)
print(21 <= 20)
print(32 >= 32)
print(1 != 2)

# Logical Operators - the return would
print(True and False)
print(False or True)
print(not False)
print(not True)

